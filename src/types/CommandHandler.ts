import { SlashCommandBuilder } from "@discordjs/builders"
import { CacheType, CommandInteraction, Guild } from "discord.js"

type WithRequired<Type, Key extends keyof Type> = Type &
	{ [Prop in Key]-?: Type[Prop] }

interface BaseCommandHandler {
	data: Omit<SlashCommandBuilder, "addSubcommand" | "addSubcommandGroup">
}
interface GuildReqCommandHandler extends BaseCommandHandler {
	guildOnly: true
	permission: "owner" | "anyone"
	execute: (
		interaction: CommandInteraction<CacheType> & {
			guild: Guild
		}
	) => Promise<true | string>
}
interface NonGuildReqCommandHandler extends BaseCommandHandler {
	guildOnly: false
	execute: (
		interaction: CommandInteraction<CacheType>
	) => Promise<true | string>
}
type CommandHandler = NonGuildReqCommandHandler | GuildReqCommandHandler
export default CommandHandler
