import { client } from "./discordObjects"
import { commands } from "./commands/index"
import { createCommands } from "./deployCommands"

createCommands()

client.once("ready", () => {
	console.log("Ready!")
})

client.on("interactionCreate", async interaction => {
	console.log("recieved:" + interaction.toString())
	if (!interaction.isCommand()) return
	const { commandName } = interaction

	//check if have commmand
	if (!commands.hasOwnProperty(commandName)) return
	const command = commands[commandName]
	if (
		command.guildOnly &&
		(interaction.channel?.type === "DM" || !interaction.guild)
	) {
		await interaction.reply("I can't execute that command inside DMs!")
		return
	}

	if (
		command.guildOnly &&
		command.permission == "owner" &&
		interaction.guild &&
		interaction.user.id != interaction.guild.ownerId
	) {
		await interaction.reply(
			"Only the server owner can execute that command!"
		)
		return
	}

	//run command
	try {
		//@ts-expect-error
		const commandReturn = await command.execute(interaction)
		if (typeof commandReturn === "string") {
			//error
			await interaction.reply({ content: commandReturn, ephemeral: true })
			return
		}
	} catch (error) {
		console.log(error)
		await interaction.reply(
			"there was an error trying to execute that command!"
		)
		return
	}
})

client.login(process.env.DISCORD_TOKEN)
