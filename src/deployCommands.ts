import { REST } from "@discordjs/rest"
import { Routes } from "discord-api-types/v9"
import { applicationId } from "./config"
import { commands } from "./commands/index"

const slashCommands = Object.values(commands).map(command => {
	return command.data.toJSON()
})
//@ts-expect-error
const rest = new REST({ version: "9" }).setToken(process.env.DISCORD_TOKEN)

export function createCommands(): void {
	rest.put(Routes.applicationCommands(applicationId), {
		body: slashCommands
	})
		.then(() =>
			console.log("Successfully registered application commands.")
		)
		.catch(console.error)
}
