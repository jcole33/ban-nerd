import Discord, { Intents } from "discord.js"
const myIntents = new Intents()
myIntents.add([
	Intents.FLAGS.DIRECT_MESSAGES,
	Intents.FLAGS.GUILD_MESSAGES,
	Intents.FLAGS.GUILDS
])
const client = new Discord.Client({
	intents: [Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILDS]
})

export { client }
