import { SlashCommandBuilder } from "@discordjs/builders"
import CommandHandler from "../types/CommandHandler"

const ban: CommandHandler = {
	data: new SlashCommandBuilder()
		.setName("cat-ban-pat")
		.setDescription("Pat the ban cat"),
	guildOnly: true,
	permission: "anyone",
	execute: async function(interaction) {
		const responseList = [
			"Purr",
			"Puuurrrr",
			"Meow",
			"Nya",
			"Meeooww?",
			"Mew",
			"***Headbutt***",
			"*Snuggle*",
			"*Nestle*"
		]
		await interaction.reply(
			responseList[Math.floor(Math.random() * responseList.length)]
		)
		return true
	}
}

export default ban
