import ban from "./ban"
import banInfo from "./ban-info"
import banPat from "./ban-pat"
import CommandHandler from "../types/CommandHandler"
import { SlashCommandBuilder } from "@discordjs/builders"

const commands: Record<string, CommandHandler> = {
	"cat-ban": ban,
	"cat-ban-info": banInfo,
	"cat-ban-pat": banPat
}

const banHelp: CommandHandler = {
	data: new SlashCommandBuilder()
		.setName("cat-ban-help")
		.setDescription("Lists available commands")
		.addStringOption(option =>
			option
				.setName("command")
				.setDescription("Command to get info on")
				.addChoices(
					//@ts-expect-error
					Object.values(commands)
						.map(command => [command.data.name, command.data.name])
						.concat([["cat-ban-help", "cat-ban-help"]])
				)
				.setRequired(false)
		),
	guildOnly: false,
	execute: async function(interaction) {
		const data = []
		const commandName = interaction.options.getString("command")
		if (!commandName) {
			data.push(
				"Here's a list of all available commands: " +
					Object.values(commands)
						.map(command => command.data.name)
						.join(", ")
			)
			data.push(
				"You can send /cat-ban-help <command name>' to get info on a specific command!"
			)
			await interaction.reply({
				content: data.join("\n"),
				ephemeral: true
			})
			return true
		}
		const command = commands[commandName]
		if (!command) {
			return "that's not a valid command!"
		}
		data.push("Name: " + command.data.name)
		data.push("Description: " + command.data.description)
		if (command.guildOnly) {
			data.push("Server Only: true")
			data.push("Permission: " + command.permission)
		}
		interaction.reply({ content: data.join("\n"), ephemeral: true })
		return true
	}
}
Object.assign(commands, { "cat-ban-help": banHelp })
export { commands }
