import CommandHandler from "../types/CommandHandler"
import pool from "../db"
import { SlashCommandBuilder } from "@discordjs/builders"
import { client } from "../discordObjects"

const banInfo: CommandHandler = {
	data: new SlashCommandBuilder()
		.setName("cat-ban-info")
		.setDescription(
			"Get the ban leader board or a specific user's ban information"
		)
		.addUserOption(option =>
			option
				.setName("target")
				.setDescription("User to get ban info about")
				.setRequired(false)
		),
	guildOnly: true,
	permission: "anyone",
	execute: async function(interaction) {
		const taggedUser = interaction.options.getUser("target")
		if (!taggedUser) {
			const countResponse = await pool.query(
				"SELECT * FROM banned_user WHERE guild_id = $1::bigint ORDER BY ban_count DESC LIMIT 10",
				[interaction.guild.id]
			)
			if (countResponse.rowCount == 0) {
				interaction.reply("No one has been banned yet!")
			} else {
				const boardPromise = countResponse.rows.map(
					async (
						// eslint-disable-next-line @typescript-eslint/camelcase
						row: { user_id: any; ban_count: string },
						index: number
					) => {
						const fetchResponse = await interaction.guild.members.fetch(
							row.user_id
						)
						let username = row.user_id
						if (fetchResponse && fetchResponse.nickname) {
							username = fetchResponse.nickname
						} else {
							const cacheResponse = await client.users.cache.get(
								row.user_id
							)
							if (cacheResponse) username = cacheResponse.username
						}
						return (
							"" +
							(index + 1) +
							". " +
							username +
							" " +
							row.ban_count
						)
					}
				)
				const board = await Promise.all(boardPromise)
				await interaction.reply("Ban Leader Board\n" + board.join("\n"))
			}
			return true
		} else {
			const countResponse = await pool.query(
				"SELECT * FROM banned_user WHERE user_id = $1::bigint AND guild_id = $2::bigint",
				[taggedUser.id, interaction.guild.id]
			)
			await interaction.reply(
				taggedUser.username +
					" has been banned " +
					(countResponse.rowCount > 0
						? countResponse.rows[0].ban_count
						: 0) +
					" times"
			)
			return true
		}
	}
}

export default banInfo
