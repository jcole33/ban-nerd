import CommandHandler from "../types/CommandHandler"
import pool from "../db"
import { SlashCommandBuilder } from "@discordjs/builders"

const ban: CommandHandler = {
	data: new SlashCommandBuilder()
		.setName("cat-ban")
		.setDescription("Ban someone")
		.addUserOption(option =>
			option
				.setName("target")
				.setDescription("User to ban")
				.setRequired(true)
		),
	guildOnly: true,
	permission: "owner",
	execute: async function(interaction) {
		let reverse = false
		let amountToBan = 1
		let toBeBanned = interaction.options.getUser("target")
		if (!toBeBanned) return "You need to tag a user to ban!"
		if (toBeBanned.id == "742677826905309315") {
			toBeBanned = interaction.user
			amountToBan = 100
			reverse = true
		}
		const countResponse = await pool.query(
			"SELECT * FROM banned_user WHERE user_id = $1::bigint AND guild_id = $2::bigint",
			[toBeBanned.id, interaction.guild.id]
		)
		let finalResponse
		if (countResponse.rowCount == 0) {
			finalResponse = await pool.query(
				"INSERT INTO banned_user(user_id, guild_id, ban_count) VALUES($1::bigint, $2::bigint, " +
					amountToBan +
					") RETURNING ban_count",
				[toBeBanned.id, interaction.guild.id]
			)
		} else {
			finalResponse = await pool.query(
				"UPDATE banned_user SET ban_count = ban_count + " +
					amountToBan +
					" WHERE user_id = $1::bigint AND guild_id = $2::bigint RETURNING ban_count",
				[toBeBanned.id, interaction.guild.id]
			)
		}
		await interaction.reply(
			(reverse
				? "You would dare turn on one of your keys to power?\n\nThe tyrant is a child of Pride\nWho drinks from their sickening cup\nRecklessness and vanity,\nUntil from their high crest headlong\nThey plummet to the dust of hope.\n-Sophocles\n\nVery well........ *PERISH*\n"
				: "") +
				toBeBanned.username +
				" has been banned " +
				finalResponse.rows[0].ban_count +
				" times"
		)
		return true
	}
}

export default ban
