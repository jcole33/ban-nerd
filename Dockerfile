FROM node:17
VOLUME /tmp

COPY dist/ /app/

RUN cd app && npm i --only=prod
CMD ["node", "app/index.js"]
