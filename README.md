# Ban Nerd

On a friend's discord server they will sometimes jokingly threaten to ban someone (usually after someone makes a joke at their expense). We started keeping count of how many times it has happened to each person on the server sort of as an anti-leader board. In order to aid that I made a discord bot to do it for us.

This bot is built in Node.js and Typescript and utilizes a PostgreSQL database. While this small project certainly didn't need it, I took the opportunity to develop a more advanced development pipeline that involved Docker and Bitbucket Pipelines that I could utilize in future projects.
